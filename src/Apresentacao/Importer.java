package Apresentacao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

import Negocio.Parametros;
import Persistencia.ImportTrajetories;

public class Importer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		System.out.println("Informe o caminho completo do arquivo de trajetorias.");
		String path = teclado.nextLine();
		
		Parametros p = Parametros.GetInstance();
		p.databaseName = "Spatial_Database";
		p.username = "postgres";
		p.password = "postgres";
		p.trackPath = "C:/Users/Win7/Dropbox/Mestrado-UFSC/Dev/Workspace/Dados/MyTracks/Condominio/";
//		p.trackPath = "C:/Users/Win7/Dropbox/Mestrado-UFSC/Dev/Workspace/Dados/MyTracks/Alisson_trajectories/";
//		p.trackPath = path;
//		p.tableTrajectoriesName = "trajetorias_alisson";
		p.tableTrajectoriesName = "traj_condominio";
		
		//C:\Users\bravi\Dropbox\Mestrado-UFSC\Dev\Workspace\AvoidanceData\MyTracks
		try {
			ImportTrajetories importer = new ImportTrajetories(p, "standards.txt");
			for(String message : importer.GetErrorMessages()){
				System.out.println(message);
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

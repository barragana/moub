package Apresentacao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import Negocio.*;

public class MOUB {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Parametros p = Parametros.GetInstance();
		p.databaseName = "Spatial_Database";
		p.username = "postgres";
		p.password = "postgres";
		
		p.tableObjectCoordinatesName = "obj_casa";
		p.tableTrajectoriesName = "traj_condominio";
		p.objectCoveringRadius = 7;
		p.regionInterestRadius = 21;
		p.minimumTrajectoryDirectedObjet = 5;
		
//		p.tableObjectCoordinatesName = "obj_alisson";
//		p.tableTrajectoriesName = "trajetorias_alisson";
//		p.objectCoveringRadius = 10;
//		p.regionInterestRadius = 40;
//		p.minimumTrajectoryDirectedObjet = 4;
		
		p.durationHeuristic = 3;
		p.speedHeuristic = 3;
		p.maxAngleForReturn = 45;
//		p.additionalClause = " and target_gid = 1 and tid not in (22,21) ";
		
		try {			
						
			TrajectoryDataManipulator ranking = new TrajectoryDataManipulator(p);

			Date inicio = new Date();
			System.out.println("\nProcedimento iniciado...\n");
			
			ranking.CreateAndConfigureTables();
			
			System.out.println("Parsing sub-trajectories...\n");
			ranking.ParseSubtrajectoriesIntoROI();
			
			System.out.println("Computing Escape speed and Surround duration thresholds...\n");
			ranking.ComputeThresholds();
			
			System.out.println("Computing patterns and local score of sub-trajectories per target object...");
			ranking.ComputeLocalScoresAndPatterns();
			
			
			for (Map.Entry<Integer, ArrayList<UnusualBehavior>> entry : ranking.GetUnusualBehaviorsPerTrajectory())
			{
				System.out.println("\nUnusual Behaviors of trajectory "+((ArrayList<UnusualBehavior>)entry.getValue()).get(0).tid);
				System.out.println("Target Object - Local Score - Unusual Behavior - ");
				for(UnusualBehavior u : entry.getValue()){
					System.out.println(String.format("      %d      -   %.2f%%    -    %s", 
							u.obstId, u.localScore*100, u.unusualBehavior));
				}
			}
			
			System.out.println("\nComputing weight of all target objects...\n");
			ranking.UpdateTargetObjectsWeights();
			
			System.out.println("Computing percentage sub-trajectory per unusual behavior per target object...\n");
			ranking.ComputePercentages();
			
			System.out.println("Ranking trajectories...\n");
			ranking.ComputeTrajectoriesScore();			
			
			System.out.println("Ranking...\n");
			System.out.println("tid - global unusual behavior");
			ArrayList<UnusualBehavior> rank = ranking.GetRanking();
			String line = "  %d - %.2f%%";
			for (UnusualBehavior unusualBehavior : rank) {
				System.out.println(String.format(line, unusualBehavior.tid, unusualBehavior.localScore*100));
			}
			
			System.out.print(ranking.GetErrorMessage());
			System.out.println(ranking.GetDisregardedDataMessage());
            
            System.out.println("Procedimento finalizado com sucesso...\n");
            Date fim = new Date();
            System.out.println("Tempo de execu��o: "+(fim.getTime() - inicio.getTime())+"ms");
        
    		//MOUBFrame lc = new MOUBFrame(avoidance.ExtractBehaviors()); 
    		//lc.CreateFrame();
    		
			ranking.Close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

}


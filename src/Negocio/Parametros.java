package Negocio;

public class Parametros {
	public String tableTrajectoriesName, tableObjectCoordinatesName;
	public int objectCoveringRadius, regionInterestRadius, 
	minimumTrajectoryDirectedObjet, durationHeuristic, speedHeuristic, maxAngleForReturn;
	public String databaseName, username, password, trackPath, additionalClause;
	private static Parametros p;
	
	
	public static Parametros GetInstance(){
		if(p ==  null )
			p = new Parametros();
		return p;
	}
}

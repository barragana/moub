package Negocio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;





public class SubTrajetoryIntoROI {
	
	public int firstGID = 0;
    public int lastGid = 0;
    public double distanciaBetweenFirstAndLast = 0;
    public String firstTime = "";
    public String lastTime = "";
    public double trajectoryDistance = 0;
    public boolean hasTrajectoryDirected = false;
	public float duration;
	public double speed;
    private SimpleDateFormat ds1;
    private SimpleDateFormat ds2;
    private String regex = "[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2} [0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}-[0-9]{1,2}";
	
	
	public SubTrajetoryIntoROI(){
		ds1 = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss.SSS");
		ds2 = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss-SS");
	}
	
	public float GetInterval()
	{
		if(this.duration > 0)
			return this.duration;
		if(firstTime.equals("") || lastTime.equals(""))
			return 0;
		try {
			Date start = firstTime.matches(regex) ? ds2.parse(firstTime) : ds1.parse(firstTime);
			Date end = lastTime.matches(regex) ? ds2.parse(lastTime) : ds1.parse(lastTime);
			float msInterval = end.getTime()-start.getTime();
			return (msInterval > 0) ? msInterval/1000 : 0; 
		} catch (ParseException e) {
			System.out.println("getInterval. Error parsing date.");
			e.printStackTrace();
		}
		return 0;
	}
	
	public double GetSpeed()
	{
		if(this.speed > 0)
			return this.speed;
		return (GetInterval() > 0) ? Float.parseFloat(trajectoryDistance+"")/GetInterval(): 0;
	}
	
	public void ClearData(){
		this.firstGID = 0;
		this.lastGid = 0;
		this.distanciaBetweenFirstAndLast = 0;
		this.firstTime = "";
		this.lastTime = "";
		this.hasTrajectoryDirected = false;
		this.duration = 0;
		this.speed = 0;
	}
}

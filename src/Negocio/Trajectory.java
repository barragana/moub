package Negocio;

import java.util.ArrayList;

public class Trajectory {
	private ArrayList<UnusualBehavior> ub;
	private double globalScore;
	private int tid;
	
	public Trajectory(int tid) {
		this.tid = tid;
	}
	
	public void addUnusualBehavior(UnusualBehavior ub){
		this.ub.add(ub);
	}
	
	public void removeUnusualBehavior(UnusualBehavior ub){
		this.ub.remove(ub);
	}
	
	public ArrayList<UnusualBehavior> getUb() {
		return ub;
	}

	public void setUb(ArrayList<UnusualBehavior> ub) {
		this.ub = ub;
	}

	public double getGlobalScore() {
		return globalScore;
	}

	public void setGlobalScore(double globalScore) {
		this.globalScore = globalScore;
	}

	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}
	
	
}

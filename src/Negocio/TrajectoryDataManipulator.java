package Negocio;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import Persistencia.*;

public class TrajectoryDataManipulator {

	private DBHelper dbHelper;
	private int minimumTrajectoryDirectedObjet;
	private String errorMessage = "";
	private String disregardedData = "";
	private Map<Integer, ArrayList<UnusualBehavior>> map;
	private ArrayList<UnusualBehavior> trajectoryUnusualBehaviors;
	
	public TrajectoryDataManipulator(Parametros p) throws SQLException, ClassNotFoundException {
		dbHelper= new DBHelper(p);
		this.minimumTrajectoryDirectedObjet = p.minimumTrajectoryDirectedObjet;
		this.map = new HashMap<Integer, ArrayList<UnusualBehavior>>();
	}
	
	public void CreateAndConfigureTables() throws SQLException{
		System.out.println("Creating main tables...\n");
		this.dbHelper.CreateMainTable();
		
		System.out.println("Creating trajectories data tables...\n");
		this.dbHelper.CreateTrajDataTable();
		
		System.out.println("Identifying sub-trajectories per trajectory per target object that intersect ROI...\n");
		this.dbHelper.InsertTrajectoryDataIntoROI();
		
		this.dbHelper.FractureTrajectoriesHasRecurrence();
		
		//this.dbHelper.RemoveCoverginRegionWhichAreNotInterceptedForAnyTrajectory();
	}
	
	public void UpdateTargetObjectsWeights() throws SQLException{
		this.dbHelper.UpdateTargetObjectsWeights();
	}
	
	public void ComputeThresholds() throws SQLException{
		this.dbHelper.ComputeThresholds(Parametros.GetInstance().speedHeuristic, Parametros.GetInstance().durationHeuristic);
	}
	
	
	public void ParseSubtrajectoriesIntoROI() throws SQLException{
		ResultSet subtrajectoriesIntoROI = this.dbHelper.GetTwoPointsOfTrajectoryIntoROI();
		int trajectoryId = 0;
		int objectId = 0;
		int tidRecurrence = 0;
		boolean interseptCoveringRegion = false;
		
		SubTrajetoryIntoROI subTrajetoryIntoROI = new SubTrajetoryIntoROI();
        SubTrajetoryIntoROI subTrajetoryIn = new SubTrajetoryIntoROI();
        SubTrajetoryIntoROI subTrajetoryOut = new SubTrajetoryIntoROI();
        
		while(subtrajectoriesIntoROI.next()){
			trajectoryId = subtrajectoriesIntoROI.getInt("tid");
			objectId = subtrajectoriesIntoROI.getInt("target_gid");
			tidRecurrence = subtrajectoriesIntoROI.getInt("tid_recurrence");
			interseptCoveringRegion = subtrajectoriesIntoROI.getBoolean("getincoveringregion");
			subTrajetoryIntoROI.firstGID = subtrajectoriesIntoROI.getInt("s_first_gid");
			subTrajetoryIntoROI.lastGid = subtrajectoriesIntoROI.getInt("s_last_gid");
			
			if(interseptCoveringRegion){
				ParseSS(objectId, tidRecurrence, subTrajetoryIn, subTrajetoryOut, subTrajetoryIntoROI);
			}
			else {
			
				IdentifySubTrajetoryGetInROI(trajectoryId, objectId, tidRecurrence, subTrajetoryIn);
				
				IdentifySubTrajetoryGetOutROI(subTrajetoryOut, subTrajetoryIn.lastGid, subTrajetoryIntoROI.lastGid);
				
			}
			this.dbHelper.UpdateTrajectoryData(trajectoryId, objectId, tidRecurrence, subTrajetoryIn, subTrajetoryOut, subTrajetoryIntoROI, !interseptCoveringRegion);
			
            subTrajetoryIn.ClearData();
            subTrajetoryOut.ClearData();
            subTrajetoryIntoROI.ClearData();
		}	
	}
	
	private void ParseSS(int objectId, int tidRecurrence, SubTrajetoryIntoROI sin, SubTrajetoryIntoROI sout, SubTrajetoryIntoROI s) throws SQLException{
		ResultSet parsedSS = this.dbHelper.ParseSS(objectId, tidRecurrence);
		if(parsedSS.next()){
			sin.distanciaBetweenFirstAndLast = parsedSS.getDouble("sin_dist");
			sin.firstGID = parsedSS.getInt("sin_first_gid");
			sin.lastGid = parsedSS.getInt("sin_last_gid");
			sin.trajectoryDistance = parsedSS.getDouble("sin_dist");
			sin.speed = parsedSS.getDouble("sin_speed");
			sin.duration = parsedSS.getFloat("sin_duration");
			
			sout.distanciaBetweenFirstAndLast = parsedSS.getDouble("sout_dist");
			sout.firstGID = parsedSS.getInt("sout_first_gid");
			sout.lastGid = parsedSS.getInt("sout_last_gid");
			sout.trajectoryDistance = parsedSS.getDouble("sout_dist");
			sout.speed = parsedSS.getDouble("sout_speed");
			sout.duration = parsedSS.getFloat("sout_duration");
			
			s.distanciaBetweenFirstAndLast = parsedSS.getDouble("s_dist");
			s.firstGID = parsedSS.getInt("s_first_gid");
			s.lastGid = parsedSS.getInt("s_last_gid");
			s.trajectoryDistance = parsedSS.getDouble("s_dist");
			s.speed = parsedSS.getDouble("s_speed");
			s.duration = parsedSS.getFloat("s_duration");
		}
	}
	
	private void IdentifySubTrajetoryGetInROI(int trajectoryId, int objectId, int tidRecurrence, SubTrajetoryIntoROI st) throws SQLException{
		ResultSet idOfEachPointOfTrajetoryIntoROI = this.dbHelper.GetIdAndTimeOfEachPointOfATrajectoryIntoROI(trajectoryId, objectId, tidRecurrence);
		int currentGid = 0; 
		String startTime = "";
		String endTime = "";
		int nextGid = 0;
		boolean intersects = false;
		double distanceBetweenTwoPoints = 0;
		
		if (idOfEachPointOfTrajetoryIntoROI.next())
		{
        	st.firstGID = currentGid = idOfEachPointOfTrajetoryIntoROI.getInt("gid"); // para identifica��o da dist�ncia
			st.firstTime = startTime = idOfEachPointOfTrajetoryIntoROI.getString("time");
		}
		
		if (idOfEachPointOfTrajetoryIntoROI.next()) 
	    {
			nextGid = idOfEachPointOfTrajetoryIntoROI.getInt("gid"); // para identifica��o da dist�ncia
	        endTime = idOfEachPointOfTrajetoryIntoROI.getString("time");
	    }
		else
		{
			nextGid = currentGid;
			endTime = startTime;
		}
	        
	    while(idOfEachPointOfTrajetoryIntoROI.next())
	    {
	    	intersects = this.dbHelper.DoesLineInterceptObject(trajectoryId, currentGid, nextGid,objectId);
			if (intersects) {
			//identify trajectory directed to the target
				distanceBetweenTwoPoints = this.dbHelper.GetDistanceBetweenPoints(trajectoryId, currentGid, nextGid);
				if (distanceBetweenTwoPoints> st.distanciaBetweenFirstAndLast) {
					st.distanciaBetweenFirstAndLast = distanceBetweenTwoPoints;
	                st.firstGID=currentGid;
	                st.lastGid=nextGid;
	                st.firstTime = startTime;
	                st.lastTime = endTime;
	                st.hasTrajectoryDirected = true;
	    			st.trajectoryDistance = this.dbHelper.GetTrajectoryDistance(trajectoryId, currentGid, nextGid);
				}
				//Keep the first point and go forward to the next
				nextGid = idOfEachPointOfTrajetoryIntoROI.getInt("gid");
	            endTime = idOfEachPointOfTrajetoryIntoROI.getString("time");
			}
			else{
			// N�o est� na mesma dire��o do obst�culo. Ignora este ponto
				currentGid = nextGid;
	            startTime = endTime;
	            nextGid = idOfEachPointOfTrajetoryIntoROI.getInt("gid");
	            endTime = idOfEachPointOfTrajetoryIntoROI.getString("time");
			}
		}
	        
	    if(!st.hasTrajectoryDirected)
	    {
            st.lastGid=nextGid;
            st.lastTime = endTime;
			st.trajectoryDistance = this.dbHelper.GetTrajectoryDistance(trajectoryId, currentGid, nextGid);
	    }
	}
	
	private void IdentifySubTrajetoryGetOutROI(SubTrajetoryIntoROI sout, int firstGid, int lastGid) throws SQLException
	{
		ResultSet soutData = this.dbHelper.GetSubtrajectoryOfWayOut(firstGid, lastGid);
		if(soutData.next()){
			sout.distanciaBetweenFirstAndLast = soutData.getDouble("sout_dist");
			sout.firstGID = soutData.getInt("sout_first_gid");
			sout.lastGid = soutData.getInt("sout_last_gid");
			sout.trajectoryDistance = soutData.getDouble("sout_dist");
			sout.speed = soutData.getDouble("sout_speed");
			sout.duration = soutData.getFloat("sout_duration");
		}
	}
	
	public void ComputeLocalScoresAndPatterns() throws SQLException
	{
		ResultSet unusualBehaviors = this.dbHelper.GetUnusualBehavior();
		ArrayList<UnusualBehavior> subtrajectoryUnusualBehaviorsPerTarget = new ArrayList<UnusualBehavior>();
		while(unusualBehaviors.next())
		{
			boolean isUnusualBehavior = false;
			//*0.5 was add because the smallest value of avoidance is 0.5
			UnusualBehavior ub = GenericUnusualBehavior(unusualBehaviors);
			if(ub.duration > ub.durationThreshold)
			{
				UnusualBehavior surround = GenericUnusualBehavior(unusualBehaviors);
				surround.unusualBehavior = "surround";
				surround.localScore = 1 - (ub.durationThreshold/ub.duration)*0.5;
				subtrajectoryUnusualBehaviorsPerTarget.add(surround);
				isUnusualBehavior = true;
			}
			if(ub.sinDist > 0)
			{
				if(ub.speedDifference > ub.speedThreshold)
				{
					UnusualBehavior escape = GenericUnusualBehavior(unusualBehaviors);
					escape.unusualBehavior = "escape";
					escape.localScore = 1 - (ub.speedThreshold/ub.speedDifference)*0.5;
					subtrajectoryUnusualBehaviorsPerTarget.add(escape);
					isUnusualBehavior = true;
				}
				if(ub.angle <= Parametros.GetInstance().maxAngleForReturn)
				{
					UnusualBehavior returning = GenericUnusualBehavior(unusualBehaviors);
					returning.unusualBehavior = "return";
					returning.localScore = 1 - (ub.angle/Parametros.GetInstance().maxAngleForReturn)*0.5;
					subtrajectoryUnusualBehaviorsPerTarget.add(returning);
					isUnusualBehavior = true;
				}
				if(ub.sinDist >= minimumTrajectoryDirectedObjet)
				{
					UnusualBehavior avoidance = GenericUnusualBehavior(unusualBehaviors);
					boolean isStrong = this.dbHelper.ComputeConfidentialIncrementalRegion(ub.tidRecurrence, ub.obstId, ub.tid);
					avoidance.unusualBehavior = "avoidance";
					avoidance.localScore = isStrong ? 1 : 0.5;
					subtrajectoryUnusualBehaviorsPerTarget.add(avoidance);
					isUnusualBehavior = true;
				}
				
				if(!isUnusualBehavior){
					UnusualBehavior none = GenericUnusualBehavior(unusualBehaviors);
					none .unusualBehavior = "none";
					none .localScore = 0;
					this.dbHelper.UpdateToNoneBehavior(none.obstId, none.tidRecurrence);
//					subtrajectoryUnusualBehaviorsPerTarget.add(none);
				}
			}
			
			if(!subtrajectoryUnusualBehaviorsPerTarget.isEmpty()){
				for(UnusualBehavior sUB : subtrajectoryUnusualBehaviorsPerTarget){
					this.dbHelper.ComputeUnusualBehavior(sUB.tid, sUB.obstId, sUB.localScore, sUB.unusualBehavior, sUB.tidRecurrence);
					this.addUnusualBehaviorToTrajectory(sUB.tid, sUB);
				}
			}
			subtrajectoryUnusualBehaviorsPerTarget.clear();
		}
	}
	
	private UnusualBehavior GenericUnusualBehavior(ResultSet unusualBehaviors) throws SQLException
	{
		return new UnusualBehavior(
				unusualBehaviors.getInt("tid"), 
				unusualBehaviors.getInt("target_gid"), 
				unusualBehaviors.getInt("tid_recurrence"), 
				"",
				unusualBehaviors.getDouble("sin_linedist"),
				unusualBehaviors.getDouble("speed_difference"), 
				unusualBehaviors.getDouble("s_duration"),
				180-Math.abs(unusualBehaviors.getDouble("angle")),
				0,
				unusualBehaviors.getDouble("duration_threshold"),
				unusualBehaviors.getDouble("speed_threshold"));
	}
	
	private void addUnusualBehaviorToTrajectory(int tid, UnusualBehavior ub){
		if(!this.map.containsKey(tid)){
			trajectoryUnusualBehaviors = new ArrayList<UnusualBehavior>();
			trajectoryUnusualBehaviors.add(ub);
			this.map.put(tid, trajectoryUnusualBehaviors);
			return;
		}
		trajectoryUnusualBehaviors = this.map.get(tid);
		trajectoryUnusualBehaviors.add(ub);
	}
	
	public Set<Entry<Integer, ArrayList<UnusualBehavior>>> GetUnusualBehaviorsPerTrajectory(){
		return this.map.entrySet();
	}
	
	public void ComputePercentages() throws SQLException
	{
		String[] patterns = {"surround", "escape", "return", "avoidance", "none"};
		ArrayList<Integer> targetObjects = this.dbHelper.GetTargetObjects();
		for (String pattern : patterns) {
			for (int i : targetObjects) {
				this.dbHelper.UpdateSubtrajectoryPercentage(pattern, i);
			}
		}
	}
	
	public void ComputeTrajectoriesScore() throws SQLException
	{
		this.dbHelper.ComputeTrajectoriesScore();
	}
	
	public ArrayList<UnusualBehavior> GetRanking() throws SQLException{
		ResultSet rankingData = this.dbHelper.GetRanking();
		ArrayList<UnusualBehavior> ranking = new ArrayList<UnusualBehavior>();
		while(rankingData.next())
		{
			ranking.add(new UnusualBehavior(rankingData.getInt("tid"), rankingData.getDouble("global_score")));
		}
		return ranking;
	}

	public void Close() throws SQLException{
		this.dbHelper.Close();
	}
	
	public String GetErrorMessage(){
		return this.errorMessage;
	}
	
	public String GetDisregardedDataMessage(){
		return this.disregardedData;
	}
}

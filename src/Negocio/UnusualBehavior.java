package Negocio;

public class UnusualBehavior {

	public int tid, obstId, tidRecurrence;
	public String unusualBehavior;
	public double speedDifference, duration, angle, sinDist, localScore, durationThreshold, speedThreshold;
	
	public UnusualBehavior(int tid, int obstId, int tidRecurrence,
			String unusualBehavior, double sinDist, double speedDifference, double duration, double angle, 
			double localScore, double durationThreshold, double speedThreshold) {
		super();
		this.tid = tid;
		this.obstId = obstId;
		this.tidRecurrence = tidRecurrence;
		this.unusualBehavior = unusualBehavior;
		this.sinDist = sinDist;
		this.speedDifference = speedDifference;
		this.duration = duration;
		this.angle = angle;
		this.localScore = localScore;
		this.durationThreshold = durationThreshold;
		this.speedThreshold = speedThreshold;
	}
	
	public UnusualBehavior(int tid, double localScore) {
		super();
		this.tid = tid;
		this.localScore = localScore;
	}
}

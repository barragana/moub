package Persistencia;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;

public class DBConnection {
   
	private static Connection CON;
	private static DBConnection DATABASE;
	
	private String databaseName, username, password;
	
	private DBConnection(String databaseName, String username, String password) throws SQLException, ClassNotFoundException{
		this.databaseName = databaseName;
		this.username = username;
		this.password = password;
		CON = GetConncetion();
	}
	
	
	//////////////////////////////////
	//			CONNECTION			//
	public static DBConnection GetInstance(String databaseName, String username, String password) throws SQLException, ClassNotFoundException{
		if(DATABASE ==  null )
			DATABASE = new DBConnection(databaseName, username, password);
		return DATABASE;
	}
	public Connection GetConncetion() throws ClassNotFoundException, SQLException{
		Class.forName("org.postgresql.Driver");
		return DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+this.databaseName,username, password);
	}
	public Statement CreateStatement() throws SQLException{
		return this.CON.createStatement();
	}
	public Statement CreateStatementScrollInsensitive(String sql) throws SQLException{
		return this.CON.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	}
	public void CloseConnection() throws SQLException{
		CON.close();
	}
	//				CONNECTION		//
	//////////////////////////////////
	
	//////////////////////////////////
	//			SQLHelper			//
	public void CreateTable(String tableName, String columns) throws SQLException{
		Statement createTable = this.CreateStatement();
		String sql = "DROP TABLE IF EXISTS "+tableName;
		createTable.execute(sql);
		sql= "Create table "+tableName+"("+columns+")";
		createTable.execute(sql);
	}
	
	public void AddGeometryColumnToTable(String tableName, String columnName, String type, String dimension) throws SQLException{
		Statement addGEOMColumn = this.CreateStatement();
		addGEOMColumn.executeQuery("select addgeometrycolumn('"+tableName+"','"+columnName+"',-1,'"+type+"',"+dimension+")"); 
	}
	
	//This is just a test to find out how to create a procedure on postgresql that is called as function
	public void ExecuteFunction(String functionName) throws SQLException{
		//Como chamar uma store procedure que em postgresql � uma function()
		CallableStatement function =  CON.prepareCall(String.format("{call %s}",functionName));
		function.execute();
		function.close();
	}

	
	
}
package Persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Negocio.Parametros;
import Negocio.SubTrajetoryIntoROI;
import Negocio.UnusualBehavior;

public class DBHelper {



	private static String UNUSUAL_BEHAVIORS_TABLE_NAME = "unusual_behaviors";
	private static String UNUSUAL_BEHAVIORS_TABLE_COLUMNS = 
			"tid int8, target_gid int8, tid_recurrence int8, pattern text, local_score double precision, percentage double precision, weight double precision";
	
	private static String RANKING_TABLE_NAME = "ranking";
	private static String RANKING_TABLE_COLUMNS = "tid int8, global_score double precision";
	
	private static String TRAJ_DATA_TABLE_NAME = "traj_data";
	private static String TRAJ_DATA_COLUMNS = "tid int8, target_gid int8, nobj integer, ponderacao_w float, getincoveringregion bool, hasrecurrence bool, tid_recurrence serial, " +
											"s_first_gid int8, s_last_gid int8, s_distance float, s_speed float, s_duration float, " +
											"localconf float, desclocalconf varchar(6), ntraj integer, " +
											"sin_first_gid int8, sin_last_gid int8, sin_linedist float, sin_dist float, sin_duration float, sin_speed float, " +
											"sout_first_gid int8, sout_last_gid int8, sout_linedist float, sout_dist float, sout_duration float, sout_speed float, " +
											"angle float, escape bool, surround bool, is_unusual_behavior bool";
	
	private static String RECURRENT_QUERY = "select * from "+ TRAJ_DATA_TABLE_NAME + " where hasrecurrence order by tid";
	
	private static String HANDLE_DATA_TRAJECTORIES_INTERCEPT_ROIS = "handle_data_trajectories_intercept_rois";
	private static String UPDATE_TARGET_OBJECTS_WEIGHTS = "update_target_objects_weights";
	private static String GET_DIRECTION_CHANGING = "get_direction_changing";
	private static String PARSE_SS = "parse_ss";
	private static String COMPUTE_THRESHOLDS = "compute_thresholds";
	private static String UPDATE_PERCENTAGE = "update_percentage";
	private static String COMPUTE_TRAJECTORIES_SCORE = "compute_trajectories_score";
	private static String COMPUTE_CONFIDENTIAL_INCREMENTAL_REGION = "compute_confidential_incremental_region";
	
	private static String EXECUTE_UPDATE_PERCENTAGE = UPDATE_PERCENTAGE+"('%s', %d)";
	private static String EXECUTE_GET_DIRECTION_CHANGING = GET_DIRECTION_CHANGING+"(%d, %d, %d, %d)";
	private static String EXECUTE_PARSE_SS = "select * from "+PARSE_SS+"(%d, %d)";
	private static String EXECUTE_UPDATE_TARGET_OBJECTS_WEIGHTS = UPDATE_TARGET_OBJECTS_WEIGHTS+"()";
	private static String EXECUTE_HANDLE_DATA_TRAJECTORIES_INTERCEPT_ROIS = HANDLE_DATA_TRAJECTORIES_INTERCEPT_ROIS+"()";
	private static String EXECUTE_COMPUTE_THRESHOLDS = COMPUTE_THRESHOLDS+"(%d,%d)";
	private static String EXECUTE_COMPUTE_TRAJECTORIES_SCORE = COMPUTE_TRAJECTORIES_SCORE+"()";
	private static String EXECUTE_COMPUTE_CONFIDENTIAL_INCREMENTAL_REGION = "select "+COMPUTE_CONFIDENTIAL_INCREMENTAL_REGION+"(%d,%d,%d)";
			
	private static String PARSE_TO_SECONDS =  "date_part('hour',(%s))*360 + " +
												"date_part('minute',(%s))*60 + " +
												"date_part('seconds',(%s))";
	
	private static String INSERT_FRACTURED_TRAJECTORY_DATA_QUERY = 
			"insert into %s (tid, target_gid, s_first_gid, s_last_gid, s_line, ponderacao_w, " +
			"s_distance, s_duration, s_speed, getincoveringregion, hasrecurrence) " +
			"(" 
			+ "select %d, "
			+ "%d, "
			+ "%d as s_first_gid, "
			+ "%d as s_last_gid, "
			+ "st_makeline(t.the_geom),"
			+ "0.0, " +
			"ST_Length(st_makeline(t.the_geom))*(%s) as s_distance, "+
			String.format(PARSE_TO_SECONDS, "max(t.time)-min(t.time)", "max(t.time)-min(t.time)", "max(t.time)-min(t.time)")+" as s_duration, "+
			"(ST_Length(st_makeline(t.the_geom))*(%s))/" +
				"(" +
					String.format(PARSE_TO_SECONDS, "max(t.time)-min(t.time)", "max(t.time)-min(t.time)", "max(t.time)-min(t.time)")+
				"+0.0001) as s_speed, "+
			"st_intersects(st_makeline(t.the_geom), %s)," +
			"true " +
			"from %s t where t.gid >= %d and t.gid <= %d"+
			");";

	private static String DELETE_RECURRENT_TRAJECTORY_DATA_QUERY = 
			"delete from %s where s_first_gid = %d and s_last_gid = %d;";
	
	private Statement getTrajectoriesIntoROI, 
						getTrajectoriesDataIntoROI, 
						getSubTrajectoryMeasures, 
						getUnusualBehaviorsInformation, 
						getResults, 
						getDistanceSphere;
	
	private double distanceSphere = 0;
	private double NormalizedCoveringRegionDistance = 0;
	private double NormalizedInterestRegionDistance = 0;
	
	private DBConnection banco;
	private Parametros parametros;
	
	private String TARGET_OBJECT_TABLE_NAME = "";
	private String REAL_TRAJ_TABLE_NAME = "";
	
	public DBHelper(Parametros p) throws SQLException, ClassNotFoundException {
		this.parametros = p;
		this.TARGET_OBJECT_TABLE_NAME = this.parametros.tableObjectCoordinatesName;
		this.REAL_TRAJ_TABLE_NAME = this.parametros.tableTrajectoriesName;
		this.banco = DBConnection.GetInstance(p.databaseName, p.username, p.password);
		this.getUnusualBehaviorsInformation = this.banco.CreateStatement();
		this.getResults = this.banco.CreateStatement();
		this.getSubTrajectoryMeasures = this.banco.CreateStatement();
		this.getTrajectoriesIntoROI = this.banco.CreateStatement();
		this.getTrajectoriesDataIntoROI = this.banco.CreateStatement();
		this.getDistanceSphere = this.banco.CreateStatement();
		this.setDistanceSphere();
		this.setNormalizedCoveringRegionDistance();
		this.setNormalizedInterestRegionDistance();
		
		this.CreateFunctionInsertTrajetoryDataIntoROI();
		this.CreateFunctionGetDirectionChanging();
		this.CreateTypeSubTrajectory();
		this.CreateFunctionParseSS();
		this.CreateFunctionUpdateTargetObjectsWeights();
		this.CreateFunctionComputeThresholds();
		this.CreateFunctionUpdatePercentage();
		this.CreateFunctionComputeTrajectoriesScore();
		this.CreateFunctionToComputeConfidentialIncrementalRegion();
	}
	
	public void CreateMainTable() throws SQLException{
		this.banco.CreateTable(UNUSUAL_BEHAVIORS_TABLE_NAME, UNUSUAL_BEHAVIORS_TABLE_COLUMNS);
//		this.banco.AddGeometryColumnToTable(UNUSUAL_BEHAVIORS_TABLE_NAME, "reginteresse", "POLYGON", "2");
//		this.banco.AddGeometryColumnToTable(UNUSUAL_BEHAVIORS_TABLE_NAME, "regincrconf", "GEOMETRY", "2");

		this.banco.CreateTable(RANKING_TABLE_NAME, RANKING_TABLE_COLUMNS);
//		this.banco.CreateTable(TARGET_OBJECT_TABLE_NAME, TARGET_OBJECT_TABLE_COLUMNS);
//		this.banco.AddGeometryColumnToTable(TARGET_OBJECT_TABLE_NAME, "the_geom", "POINT", "2");
//		this.banco.AddGeometryColumnToTable(TARGET_OBJECT_TABLE_NAME, "buffer_obj", "GEOMETRY", "2");
//		this.banco.AddGeometryColumnToTable(TARGET_OBJECT_TABLE_NAME, "buffer_reg", "GEOMETRY", "2");
	}
	
	public void CreateTrajDataTable() throws SQLException{
		this.banco.CreateTable(TRAJ_DATA_TABLE_NAME, TRAJ_DATA_COLUMNS);
		this.banco.AddGeometryColumnToTable(TRAJ_DATA_TABLE_NAME, "s_line", "LINESTRING", "2");
		this.banco.AddGeometryColumnToTable(TRAJ_DATA_TABLE_NAME, "reginteresse", "POLYGON", "2");
		this.banco.AddGeometryColumnToTable(TRAJ_DATA_TABLE_NAME, "regincrconf", "GEOMETRY", "2");
	}

	///////////////////////////////////////////////////////////////////////////////////////
	//insert into auxiliary table one row with two points of a trajectory that intercepts//
	//the ROI. thisgid is the first one get into the ROI and nextgid is the last one     //
	//before get out ROI.  														         //
	///////////////////////////////////////////////////////////////////////////////////////
	private void CreateFunctionInsertTrajetoryDataIntoROI() throws SQLException{
		String function = "CREATE OR REPLACE FUNCTION "+HANDLE_DATA_TRAJECTORIES_INTERCEPT_ROIS+"() RETURNS void AS\n" +
				"$BODY$\n" +
				"DECLARE\n" +
					" r "+parametros.tableObjectCoordinatesName+"%rowtype;\n" +
				"BEGIN\n" +
					"FOR r IN\n" +
						"SELECT * FROM "+parametros.tableObjectCoordinatesName+"\n" +
					"LOOP\n" +
						"insert into "+TRAJ_DATA_TABLE_NAME+" (tid, target_gid, s_first_gid, s_last_gid, s_line, ponderacao_w, s_distance, s_duration, s_speed, getincoveringregion, hasrecurrence) " +
							"(select " +
								"t.tid, " +
								"o.gid as target_gid, " +
								"min(t.gid) as s_first_gid, " +
								"max(t.gid) as s_last_gid, " +
								"st_makeline(t.the_geom), " +
								"0.5,  " +
								"ST_Length(st_makeline(t.the_geom))*"+this.distanceSphere+" as s_distance, "+
								String.format(PARSE_TO_SECONDS, "max(t.time)-min(t.time)", "max(t.time)-min(t.time)", "max(t.time)-min(t.time)")+" as s_duration, "+
								"(ST_Length(st_makeline(t.the_geom))*("+this.distanceSphere+"))/" +
									"(" +
										String.format(PARSE_TO_SECONDS, "max(t.time)-min(t.time)", "max(t.time)-min(t.time)", "max(t.time)-min(t.time)")+
									"+0.0001) as s_speed, "+
								"st_intersects(st_makeline(t.the_geom), "+this.GetBufferForCoveringRegion("r.gid")+"), "+
								this.DoesTrajetoryHaveRecurrence("r.gid")+
								" from "+parametros.tableObjectCoordinatesName+" o, (select * from "+parametros.tableTrajectoriesName+" order by gid) t where " +
								"st_intersects(t.the_geom, "+ this.GetBufferForInterestRegion("r.gid")+")"+
								" and o.gid=r.gid " +
								"group by t.tid, o.gid order by t.tid, o.gid);\n" +
												
					"END LOOP;\n" +
						
					"RETURN;\n" +
				"END\n" +
				"$BODY$\n" +
				"LANGUAGE plpgsql";
		this.getTrajectoriesIntoROI.execute(function);
	}
	
	private void CreateTypeSubTrajectory() throws SQLException {
		String type = "DROP FUNCTION IF EXISTS "+PARSE_SS+"(integer, integer); "
				+ "DROP TYPE IF EXISTS SUBTRAJECTORY; "+
			"CREATE TYPE SUBTRAJECTORY as (tid int, target_gid int, recurrence_id int, "+
				"s_first_gid int8,s_last_gid int8,s_dist float,s_duration float, s_speed float, "+
				"sin_first_gid int8, sin_last_gid int8,sin_dist float,sin_duration float, sin_speed float, " +
				"sout_first_gid int8,sout_last_gid int8, sout_dist float,sout_duration float, sout_speed float, "+
				"pattern text,L float)";
		this.getTrajectoriesIntoROI.execute(type);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	//Parse standard sub-trajectory into roi into sin and sout					         //
	///////////////////////////////////////////////////////////////////////////////////////
	private void CreateFunctionParseSS() throws SQLException{
	String function = "CREATE OR REPLACE FUNCTION "+PARSE_SS+"(obj_gid integer, recurrence_id integer) "
			+ "RETURNS SUBTRAJECTORY AS\n" +
			"$BODY$\n" +
				"DECLARE\n" +
					"first_in int8; " +
					"last_in int8; " +
					"result SUBTRAJECTORY;"+
				"BEGIN\n"+
					"select min(tc.gid), max(tc.gid) into first_in, last_in from "+this.parametros.tableTrajectoriesName+" tc "+
					"inner join "+TRAJ_DATA_TABLE_NAME+" td on td.tid = tc.tid and td.tid_recurrence = recurrence_id "+
					"where st_intersects(tc.the_geom, "+this.GetBufferForCoveringRegion("obj_gid")+") "+
					"and tc.gid >= td.s_first_gid and tc.gid <= td.s_last_gid;\n"+
					
					"select td.s_first_gid, td.s_last_gid "+
					" into result.s_first_gid,result.s_last_gid "+
					" from "+TRAJ_DATA_TABLE_NAME+" td where "+
					"td.tid_recurrence = recurrence_id;\n"+
					
					"select min(tc.gid), max(tc.gid), "+
					"ST_Length(st_makeline(tc.the_geom))*"+this.distanceSphere+" as sin_distance, "+
					String.format(PARSE_TO_SECONDS, "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)")+" as sin_duration, "+
					"(ST_Length(st_makeline(tc.the_geom))*("+this.distanceSphere+"))/" +
						"(" +
							String.format(PARSE_TO_SECONDS, "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)")+
						"+0.0001) as sin_speed "+
					" into result.sin_first_gid,result.sin_last_gid,result.sin_dist,result.sin_duration,result.sin_speed "+
					" from "+parametros.tableTrajectoriesName+"  tc " +
					"where tc.gid >= result.s_first_gid and tc.gid<first_in;\n"+
					
					"select min(tc.gid), max(tc.gid), "+
					"ST_Length(st_makeline(tc.the_geom))*"+this.distanceSphere+" as sout_distance, "+
					String.format(PARSE_TO_SECONDS, "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)")+" as sout_duration, "+
					"(ST_Length(st_makeline(tc.the_geom))*("+this.distanceSphere+"))/" +
						"(" +
							String.format(PARSE_TO_SECONDS, "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)")+
						"+0.0001) as sout_speed "+
					" into result.sout_first_gid,result.sout_last_gid,result.sout_dist,result.sout_duration,result.sout_speed "+
					" from "+parametros.tableTrajectoriesName+"  tc " +
					"where tc.gid > last_in and tc.gid<=result.s_last_gid;\n"+
					
					"result.s_duration = result.sin_duration+result.sout_duration;\n"+
					"result.s_dist = result.sin_dist+result.sout_dist;\n"+
					"result.s_speed = result.s_dist/result.s_duration;\n"+
					
					"return result;\n"+
				"END\n" +
				"$BODY$\n" +
				"LANGUAGE plpgsql";
		this.getTrajectoriesIntoROI.execute(function);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//Get angle of direction changing according to the continuous movement on the same direction //////
	///////////////////////////////////////////////////////////////////////////////////////////////////
	private void CreateFunctionGetDirectionChanging() throws SQLException{
		String function = "CREATE OR REPLACE FUNCTION "+GET_DIRECTION_CHANGING+"(sin_first integer, sin_last integer, sout_first integer, sout_last integer) RETURNS float AS\n" +
				"$BODY$\n" +
				"DECLARE\n" +
					"directionIn float := ST_Azimuth((select the_geom from "+REAL_TRAJ_TABLE_NAME+" where gid = sin_first), (select the_geom from "+REAL_TRAJ_TABLE_NAME+" where gid = sin_last))/(2*pi())*360;\n"+
					"directionOut float := ST_Azimuth((select the_geom from "+REAL_TRAJ_TABLE_NAME+" where gid = sout_first), (select the_geom from "+REAL_TRAJ_TABLE_NAME+" where gid = sout_last))/(2*pi())*360;\n"+
					"directionChanging float := directionIn - directionOut;\n"+
				"BEGIN\n"+
					"IF directionChanging > 180 THEN\n"+
				    	"directionChanging := 360 - directionChanging;\n"+
				    "ELSIF directionChanging < -180  THEN\n"+
				    	"directionChanging := 360 + directionChanging;\n"+
				    	"END IF;\n"+
				    "RETURN directionChanging;\n"+
				"END\n" +
				"$BODY$\n" +
				"LANGUAGE plpgsql";
		this.getUnusualBehaviorsInformation.execute(function);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//Calculate and update the weight of every target object in every table						 //////
	///////////////////////////////////////////////////////////////////////////////////////////////////
	private void CreateFunctionUpdateTargetObjectsWeights() throws SQLException{
		String function = "CREATE OR REPLACE FUNCTION "+UPDATE_TARGET_OBJECTS_WEIGHTS+"() RETURNS void AS\n" +
				"$BODY$\n" +
				"DECLARE\n" +
					" ss double precision;"+
					" s double precision;"+
					" r "+parametros.tableObjectCoordinatesName+"%rowtype;\n" +
				"BEGIN\n" +
					"FOR r IN\n" +
						"SELECT * FROM "+parametros.tableObjectCoordinatesName+"\n" +
					"LOOP\n" +
						"select count(*) into s from "+TRAJ_DATA_TABLE_NAME+" where target_gid = r.gid and not getincoveringregion;\n"+
						"select count(*) into ss from "+TRAJ_DATA_TABLE_NAME+" where target_gid = r.gid;\n"+
						"update "+TRAJ_DATA_TABLE_NAME+" set ponderacao_w = (1-s/ss) where target_gid = r.gid;\n"+
						"update "+UNUSUAL_BEHAVIORS_TABLE_NAME+" set weight = (1-s/ss) where target_gid = r.gid;\n"+
						"update "+parametros.tableObjectCoordinatesName+" set weight = (1-s/ss) where gid = r.gid;\n"+
					"END LOOP;\n"+
					"RETURN;\n"+
				"END\n" +
				"$BODY$\n" +
				"LANGUAGE plpgsql";
		this.getUnusualBehaviorsInformation.execute(function);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	//Compute Escape speed and Surround duration thresholds										//////
	//////////////////////////////////////////////////////////////////////////////////////////////////
	private void CreateFunctionComputeThresholds() throws SQLException{
		String function = "CREATE OR REPLACE FUNCTION "+COMPUTE_THRESHOLDS+"(speed integer, duration integer) RETURNS void AS\n" +
			"$BODY$\n" +
			"DECLARE\n" +
				"Saverage double precision;"+
				"Sdeviation double precision;\n"+
				"Daverage double precision;"+
				"Ddeviation double precision;\n"+
				" r "+parametros.tableObjectCoordinatesName+"%rowtype;\n" +
			"BEGIN\n" +
				"FOR r IN\n" +
					"SELECT * FROM "+parametros.tableObjectCoordinatesName+"\n" +
				"LOOP\n" +
					"select stddev_pop(sout_speed-sin_speed), avg(sout_speed-sin_speed), stddev_pop(s_duration), avg(s_duration) "+
					"into Sdeviation, Saverage, Ddeviation, Daverage "+ 
					"from "+TRAJ_DATA_TABLE_NAME+" where target_gid=r.gid and not is_unusual_behavior;\n"+
					"IF Saverage < 0 THEN "+
				    	"Saverage := 0; "+
				    "END IF;\n"+
					"update "+parametros.tableObjectCoordinatesName+" set speed_threshold = (Saverage + (Sdeviation*speed)), duration_threshold = (Daverage + (Ddeviation*duration)) "+
					"where gid = r.gid;\n"+
				"END LOOP;\n"+
				"RETURN;\n"+
			"END\n" +
			"$BODY$\n" +
			"LANGUAGE plpgsql";
		this.getUnusualBehaviorsInformation.execute(function);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//Update percentage per target object per unusual behavior of sub-trajectories				//////
	///////////////////////////////////////////////////////////////////////////////////////////////////
	private void CreateFunctionUpdatePercentage() throws SQLException{
		String function = "CREATE OR REPLACE FUNCTION "+UPDATE_PERCENTAGE+"(behavior text, obj_gid integer) RETURNS void AS\n" +
			"$BODY$\n" +
			"DECLARE\n" +
				" total double precision;"+ //total of s with the pattern at target_gid object
				" subObjBehavior double precision;"+ //number of s with the pattern behavior at target_gid object
				" tfor "+TRAJ_DATA_TABLE_NAME+"%rowtype;\n" +
			"BEGIN\n" +
				"select count(*) into total from "+TRAJ_DATA_TABLE_NAME+" t "+
				"inner join "+UNUSUAL_BEHAVIORS_TABLE_NAME+" u on u.target_gid = obj_gid and u.pattern = behavior "+
				"where t.tid_recurrence = u.tid_recurrence;\n"+
				"FOR tfor IN\n" +
					"SELECT distinct t.tid FROM "+TRAJ_DATA_TABLE_NAME+" t "+
					"inner join "+UNUSUAL_BEHAVIORS_TABLE_NAME+" u on u.target_gid = obj_gid and u.pattern = behavior "+
					"where t.tid = u.tid\n" +
				"LOOP\n"+
					"select count(*) into subObjBehavior from "+TRAJ_DATA_TABLE_NAME+" t "+
					"inner join "+UNUSUAL_BEHAVIORS_TABLE_NAME+" u on u.target_gid = obj_gid and u.pattern = behavior "+
					"where t.tid = tfor.tid and u.tid_recurrence = t.tid_recurrence;\n"+
					
					"update "+UNUSUAL_BEHAVIORS_TABLE_NAME+" set percentage = (subObjBehavior/total) "+
					"where tid= tfor.tid and target_gid = obj_gid and pattern = behavior;\n"+
				"END LOOP;\n"+
				"RETURN;\n"+
			"END\n" +
			"$BODY$\n" +
			"LANGUAGE plpgsql";
		this.getUnusualBehaviorsInformation.execute(function);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	//Insert into Ranking table the trajectory and its global score								//////
	//////////////////////////////////////////////////////////////////////////////////////////////////
	private void CreateFunctionComputeTrajectoriesScore() throws SQLException{
		String function = "CREATE OR REPLACE FUNCTION "+COMPUTE_TRAJECTORIES_SCORE+"() RETURNS void AS\n" +
			"$BODY$\n" +
			"BEGIN\n" +
				"INSERT into "+RANKING_TABLE_NAME+" (tid, global_score)"
				+ "(select uo.tid, "
					+ "cast(count(uo.tid) as double precision)/cast((count(uo.tid)+(select count(td.tid) from "+TRAJ_DATA_TABLE_NAME+" td where td.tid = uo.tid and not td.is_unusual_behavior)) as double precision)*"
					+ "(sum(local_score*percentage*weight/"
						+ "(select sum(weight*percentage) from "+UNUSUAL_BEHAVIORS_TABLE_NAME+" u where u.tid = uo.tid))"
					+ ") from "+UNUSUAL_BEHAVIORS_TABLE_NAME+" uo group by uo.tid);"+
				"RETURN;\n"+
			"END\n" +
			"$BODY$\n" +
			"LANGUAGE plpgsql";
		this.getUnusualBehaviorsInformation.execute(function);
	}
	
	private void CreateFunctionToComputeConfidentialIncrementalRegion() throws SQLException {
		String function = "CREATE OR REPLACE FUNCTION "+COMPUTE_CONFIDENTIAL_INCREMENTAL_REGION+"(tidRecurrence integer, targetGid integer, trajId integer) RETURNS BOOLEAN AS\n" +
				"$BODY$\n" +
				"DECLARE\n" +
					"pointBeforeGetIntoROI integer;\n"+
					"pointOnTheOtherSideOfROI text;\n"+
					"isStrong BOOLEAN;\n"+
				"BEGIN\n" +
					"select s_first_gid-1 into pointBeforeGetIntoROI from "+TRAJ_DATA_TABLE_NAME+" where tid_recurrence = tidRecurrence;\n"+
					"select st_astext" +
							"(st_geometryfromtext\n" +
								"('POINT\n" +
									"('|| cast" +
										"(st_x((select the_geom from "+this.parametros.tableObjectCoordinatesName+" where gid=targetGid)) + " +
											"("+this.NormalizedInterestRegionDistance + ") * sin" +
											"(" +
												"(st_azimuth" +
													"(" +
														"(select the_geom from "+this.parametros.tableObjectCoordinatesName+" where gid=targetGid)," +
														"(select st_intersection" +
															"(" +
																"(select st_makeline" +
																	"(" +
																		"(select the_geom from "+this.parametros.tableTrajectoriesName+" where tid=trajId and gid =pointBeforeGetIntoROI)," +
																		"(select the_geom from "+this.parametros.tableObjectCoordinatesName+" where gid=targetGid)" +
																	")" +
																"), " +
																"(select distinct st_exteriorring(reginteresse) from "+TRAJ_DATA_TABLE_NAME+" where tid=trajId and target_gid=targetGid) " +
															")" +
														")" +
													")" +
												")+pi()" +
											") as text" +
										")" +
										" || ' ' || cast" +
										"(st_y((select the_geom from "+this.parametros.tableObjectCoordinatesName+" where gid=targetGid)) + " +
											this.NormalizedInterestRegionDistance+" * " +
											"cos" +
												"(" +
													"(st_azimuth" +
														"(" +
															"(select the_geom from "+this.parametros.tableObjectCoordinatesName+" where gid=targetGid)," +
															"(select st_intersection" +
																"(" +
																	"(select st_makeline" +
																		"(" +
																			"(select the_geom from "+this.parametros.tableTrajectoriesName+" where tid=trajId and gid =pointBeforeGetIntoROI)," +
																			"(select the_geom from "+this.parametros.tableObjectCoordinatesName+" where gid=targetGid)" +
																		")" +
																	"),(select distinct st_exteriorring(reginteresse) from "+TRAJ_DATA_TABLE_NAME+" where tid=trajId and target_gid=targetGid)" +
																")" +
															")" +
														")" +
													")+pi()" +
												") as text" +
										") || " +
									"')' " +
								")" +
							") into pointOnTheOtherSideOfROI;\n"+
						"update "+TRAJ_DATA_TABLE_NAME+" set regincrconf = st_intersection " +
							"(st_difference" +
								"(st_buffer" +
									"(" +
										"(st_makeline" +
											"(" +
												"(select the_geom from "+parametros.tableObjectCoordinatesName+" where gid=targetGid)," +
												"st_geometryfromtext(pointOnTheOtherSideOfROI)" +
											")" +
										"), " +
										 this.NormalizedCoveringRegionDistance +
									"), " +
									"(select st_buffer" +
										"(the_geom, "+this.NormalizedCoveringRegionDistance +") from "+parametros.tableObjectCoordinatesName+" where gid=targetGid "+
									")" +
								"), " +
								"(select reginteresse from "+TRAJ_DATA_TABLE_NAME+" where tid=trajId and target_gid=targetGid and tid_recurrence = tidRecurrence) " +
							") " +
							"where tid=trajId and target_gid=targetGid and tid_recurrence = tidRecurrence;\n"+
					"select st_intersects(st_makeline(tc.the_geom), td.regincrconf) into isStrong from "+parametros.tableTrajectoriesName+" tc "+
							"inner join "+TRAJ_DATA_TABLE_NAME+" td on td.tid_recurrence = tidRecurrence and td.target_gid = targetGid " +
							"where tc.gid >= td.sout_first_gid and tc.gid <= td.sout_last_gid group by td.regincrconf;\n"+
					"RETURN isStrong;\n"+
				"END\n" +
				"$BODY$\n" +
				"LANGUAGE plpgsql";;
	//	System.out.println(function);
		this.getUnusualBehaviorsInformation.execute(function);
	}

	public void InsertTrajectoryDataIntoROI() throws SQLException
	{
		this.banco.ExecuteFunction(EXECUTE_HANDLE_DATA_TRAJECTORIES_INTERCEPT_ROIS);
	}
	
	public void ComputeThresholds(int speedSensitivity, int surroundSensitivity) throws SQLException
	{
		this.banco.ExecuteFunction(String.format(EXECUTE_COMPUTE_THRESHOLDS, speedSensitivity, surroundSensitivity));
	}
	
	public ResultSet ParseSS(int objId, int recurrenceId) throws SQLException{
		String sql = String.format(EXECUTE_PARSE_SS, objId, recurrenceId);
		return this.getUnusualBehaviorsInformation.executeQuery(sql);
	}
	
	public void UpdateTargetObjectsWeights() throws SQLException {
		this.banco.ExecuteFunction(EXECUTE_UPDATE_TARGET_OBJECTS_WEIGHTS);
	}
	
	public void UpdateSubtrajectoryPercentage(String pattern, int targetGid) throws SQLException{
		this.banco.ExecuteFunction(String.format(EXECUTE_UPDATE_PERCENTAGE, pattern, targetGid));
	}
	
	public void ComputeTrajectoriesScore() throws SQLException{
		this.banco.ExecuteFunction(EXECUTE_COMPUTE_TRAJECTORIES_SCORE);
	}
	
	public Boolean ComputeConfidentialIncrementalRegion(int tidRecurrence, int targetGid, int tid) throws SQLException{
		ResultSet r = this.getSubTrajectoryMeasures.executeQuery(String.format(EXECUTE_COMPUTE_CONFIDENTIAL_INCREMENTAL_REGION, tidRecurrence, targetGid, tid));
		if(r.next())
			return r.getBoolean(1);
		return false;
	}
	
	public void FractureTrajectoriesHasRecurrence() throws SQLException
	{
		ResultSet recurrentTrajectories = this.getTrajectoriesIntoROI.executeQuery(RECURRENT_QUERY);
		int tid, thisgid, nextgid, gid_obst;
		while(recurrentTrajectories.next())
		{
			tid = recurrentTrajectories.getInt("tid");
			thisgid = recurrentTrajectories.getInt("s_first_gid");
			nextgid = recurrentTrajectories.getInt("s_last_gid");
			gid_obst = recurrentTrajectories.getInt("target_gid");
			
			this.FractureTrajectory(false, gid_obst, nextgid, tid, thisgid);
			this.getResults.execute(String.format(DELETE_RECURRENT_TRAJECTORY_DATA_QUERY, TRAJ_DATA_TABLE_NAME, thisgid, nextgid));
		}
	}
	
	private boolean FractureTrajectory(boolean shouldIntersectROI, int gid_obst, int nextgid, int tid, int thisgid) throws SQLException
	{
		String sqlMinMax = InsideOutsiedSubTrajectoryIndentifierQuery(shouldIntersectROI, gid_obst, nextgid, thisgid);

		ResultSet recurrentTrajectoryMinMax = this.getSubTrajectoryMeasures.executeQuery(sqlMinMax);
		recurrentTrajectoryMinMax.next();
		int min = recurrentTrajectoryMinMax.getInt("min");
		int max = recurrentTrajectoryMinMax.getInt("max");
		String sql;
		boolean lastIteration = false;
		if(min != 0 && max !=0)
		{
			if(!shouldIntersectROI)
			{
				sql = String.format(INSERT_FRACTURED_TRAJECTORY_DATA_QUERY, TRAJ_DATA_TABLE_NAME,
						tid, gid_obst, thisgid, min-1, this.distanceSphere, this.distanceSphere, 
						this.GetBufferForCoveringRegion(""+gid_obst), this.parametros.tableTrajectoriesName, 
						thisgid, min-1);
				sql = sql.concat(String.format(INSERT_FRACTURED_TRAJECTORY_DATA_QUERY, TRAJ_DATA_TABLE_NAME, 
						tid, gid_obst, max+1, nextgid, this.distanceSphere, this.distanceSphere, 
						this.GetBufferForCoveringRegion(""+gid_obst), this.parametros.tableTrajectoriesName, 
						max+1, nextgid));
				
				this.getResults.execute(sql);
			}
			lastIteration = this.FractureTrajectory(!shouldIntersectROI, gid_obst, max, tid, min);
			if(lastIteration && shouldIntersectROI)
			{
				this.getResults.execute(
						String.format(INSERT_FRACTURED_TRAJECTORY_DATA_QUERY, TRAJ_DATA_TABLE_NAME,
						tid, gid_obst, min, max, this.distanceSphere, this.distanceSphere, 
						this.GetBufferForCoveringRegion(""+gid_obst), this.parametros.tableTrajectoriesName, 
						min, max));
			}
		}
		else
		{
			return true;
		}
		return false;
	}
	
	private String InsideOutsiedSubTrajectoryIndentifierQuery(boolean shouldIntersectROI, int gid_obst, int nextgid, int thisgid)
	{
		String sql = "select min(t.gid) as min, max(t.gid) as max from "+this.parametros.tableTrajectoriesName+ " t " +
				"where t.gid >= "+thisgid+" and t.gid <= "+nextgid+" and ";
		if(!shouldIntersectROI)
			sql = sql+"not ";
		sql = sql+ "st_intersects(t.the_geom, "+this.GetBufferForInterestRegion(""+gid_obst)+")";
		return sql;
		
	}	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//select two points of a trajectory into ROI, the first get in and the last before get out the ROI //
	//and which doesn't intercepts the objects covering region. This considers each object             //
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	public ResultSet GetTwoPointsOfTrajectoryIntoROI() throws SQLException{
		String sql = "select * from "+
						TRAJ_DATA_TABLE_NAME + " order by tid, target_gid";
        return this.getTrajectoriesIntoROI.executeQuery(sql);
	}
	
	private void setDistanceSphere() throws SQLException{
		ResultSet r = this.getDistanceSphere.executeQuery("select st_distance_sphere(st_geometryfromtext('POINT(0 0)'), st_geometryfromtext('POINT(0 1)'))");
		r.next();
		this.distanceSphere = r.getDouble(1);
		this.getDistanceSphere.close();
	}
	private void setNormalizedCoveringRegionDistance(){
		this.NormalizedCoveringRegionDistance = this.parametros.objectCoveringRadius / this.distanceSphere;
	}
	private void setNormalizedInterestRegionDistance(){
		this.NormalizedInterestRegionDistance = this.parametros.regionInterestRadius / this.distanceSphere;
	}
	
	private String SelectCoordinatesOfSpecificObjectHasTrajectoryIntoROI(String auxGidObst){
		return "(select o.the_geom from "+ this.parametros.tableObjectCoordinatesName+ " o where o.gid="+auxGidObst+")";
	}
	private String GetBufferForCoveringRegion(String auxGidObst){
		String sql = "st_buffer("+
				this.SelectCoordinatesOfSpecificObjectHasTrajectoryIntoROI(auxGidObst)+","+
				this.NormalizedCoveringRegionDistance+")";
		//System.out.println("convering= "+sql);
		return sql;
	}
	private String GetBufferForInterestRegion(String auxGidObst){
		String sql = "st_buffer("+this.SelectCoordinatesOfSpecificObjectHasTrajectoryIntoROI(auxGidObst)+", "+this.NormalizedInterestRegionDistance+")";
		//System.out.println(sql);
		return sql;
	}
	private String DoesTrajetoryHaveRecurrence(String objGid)
	{
		String sql = "not ST_Within(" +
						"(select st_makeline(tt.the_geom) from " +
							""+REAL_TRAJ_TABLE_NAME+" tt where tt.gid > min(t.gid) and tt.gid < max(t.gid) group by tt.tid order by tt.tid" +
						"), " +
						this.GetBufferForInterestRegion("r.gid")+
					")";
		return sql;
	}
	
	
	public ResultSet GetIdAndTimeOfEachPointOfATrajectoryIntoROI(int auxTid, int auxGidObst, int tidRecurrence) throws SQLException{
		String sql = "select t.gid, t.time"+
        				" from "+this.parametros.tableObjectCoordinatesName+" o, "+
        				this.parametros.tableTrajectoriesName+" t"+
        				" inner join "+TRAJ_DATA_TABLE_NAME+" a on a.tid = "+auxTid+ " and a.tid_recurrence = "+tidRecurrence+
						" where t.gid >= a.s_first_gid and t.gid <= a.s_last_gid"+
						" and t.tid= "+ auxTid+
        				" and o.gid="+auxGidObst+ // ** dentre o m�x e m�nimo, s� busca os que est�o dentro da regi�o de interesse.
        				" order by t.gid";
        return this.getTrajectoriesDataIntoROI.executeQuery(sql);
	}
	
	public ResultSet GetSubtrajectoryOfWayOut(int firstId, int lastGid) throws SQLException{
		String sql = "select min(tc.gid) as sout_first_gid, "
				+ "max(tc.gid) as sout_last_gid, "+
				"ST_Length(st_makeline(tc.the_geom))*"+this.distanceSphere+" as sout_dist, "+
				String.format(PARSE_TO_SECONDS, "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)")+" as sout_duration, "+
				"(ST_Length(st_makeline(tc.the_geom))*("+this.distanceSphere+"))/" +
					"(" +
						String.format(PARSE_TO_SECONDS, "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)", "max(tc.time)-min(tc.time)")+
					"+0.0001) as sout_speed "+
				" from "+parametros.tableTrajectoriesName+"  tc " +
				"where tc.gid > "+firstId+"and tc.gid<="+lastGid+"\n";
        return this.getTrajectoriesDataIntoROI.executeQuery(sql);
	}	
	
	private String CreateLineBetweenTwoPoints(int tid, int p1, int p2){
		String sql = 
				"st_makeline" +
				"( "+
		        	" (" +
		        		"select st_geomfromtext" +
		        		"(" +
		        			"'POINT" +
		        			"(" +
		        				"'|| cast" +
		        					"(" +
			        					"st_x(p1.the_geom) + " +
			        						"(" +
			        							"sin(st_azimuth(p1.the_geom, p2.the_geom)) "+
			        							" * "+2*this.NormalizedInterestRegionDistance+
			        						") as text" +
			        				") ||' " +
			        			"'|| cast" +
			        				"(" +
			        					"st_y(p1.the_geom) + " +
			        						"(" +
			        							"cos(st_azimuth(p1.the_geom, p2.the_geom)) "+
			        							" * "+ 2*this.NormalizedInterestRegionDistance+
			        						") as text" +
			        				") ||" +
			        		"')'" +
			        	") " +
			        	"from "+this.parametros.tableTrajectoriesName+" p1, "+this.parametros.tableTrajectoriesName+" p2 " +
			        	"where p1.gid = "+ p1 +" and p2.gid = "+ p2 +
			        "), " +
			        "(select the_geom from "+this.parametros.tableTrajectoriesName+" where gid = "+ p1 +")" +
			  ")";
		return sql;
	}
	
	public boolean DoesLineInterceptObject(int trajectoryId, int p1, int p2, int auxGidObst) throws SQLException{
		String sql = "select st_intersects("+this.CreateLineBetweenTwoPoints(trajectoryId, p1, p2)+", "+this.GetBufferForCoveringRegion(""+auxGidObst)+") as intercepta";
		ResultSet rs = this.getSubTrajectoryMeasures.executeQuery(sql);
		rs.next();
		return rs.getBoolean("intercepta");
	}
	
	public double GetDistanceBetweenPoints(int tid, int p1, int p2) throws SQLException{
		 String sql = "select st_distance_sphere(" +
		 		"(select the_geom from "+parametros.tableTrajectoriesName+" where gid="+p1+")," +
		 		"(select the_geom from "+parametros.tableTrajectoriesName+" where gid="+p2+")) as distancia_pontos ";
		ResultSet rs = this.getSubTrajectoryMeasures.executeQuery(sql);
		rs.next();
        return rs.getDouble("distancia_pontos");
	}
	
	public double GetTrajectoryDistance(int tid, int p1, int p2) throws SQLException{
		String sql = "select ST_Length(st_makeline(ARRAY(select the_geom from "+parametros.tableTrajectoriesName+" where tid = "+tid+" and gid >= "+p1+" and gid <= "+p2+"))) as dist";
		ResultSet rs = this.getSubTrajectoryMeasures.executeQuery(sql);
		rs.next();
       return rs.getDouble("dist")*this.distanceSphere;
	}
	
	public void UpdateTrajectoryData(int auxTid, int auxGidObst, 
			int tidRecurrence, SubTrajetoryIntoROI subTrajDirTar, SubTrajetoryIntoROI subTrajOut, 
			SubTrajetoryIntoROI s, boolean isUnusual) throws SQLException{
		String sql = "update "+TRAJ_DATA_TABLE_NAME+ " set "
				+ "sin_first_gid = %d, sin_last_gid = %d, sin_linedist = %s, sin_dist = %s, sin_duration = %s, sin_speed = %s, " 
				+ "sout_first_gid = %d, sout_last_gid = %d, sout_linedist = %s, sout_dist = %s, sout_duration = %s, sout_speed = %s, " 
				+ "angle = (%s), reginteresse = (select st_buffer(the_geom, "+this.NormalizedInterestRegionDistance +") from "+parametros.tableObjectCoordinatesName+" o where o.gid = "+auxGidObst+"), "
				+ "is_unusual_behavior = %s "
				+ "where tid = %d and target_gid = %d and tid_recurrence = %d;";
		
		String query = String.format(sql, 
				subTrajDirTar.firstGID, // armazena Gid inicial da subtrajet�ria direcionada ao alvo e seu tamanho
				subTrajDirTar.lastGid, // armazena Gid final da subtrajet�ria direcionada
				(subTrajDirTar.distanciaBetweenFirstAndLast), // tamanho da trajetoria em linha reta
				subTrajDirTar.trajectoryDistance,// tamanho da trajetoria
				subTrajDirTar.GetInterval(),
				subTrajDirTar.GetSpeed(),
				subTrajOut.firstGID, // armazena Gid inicial da subtrajet�ria of way out
				subTrajOut.lastGid, // armazena Gid final da subtrajet�ria of way out
				subTrajOut.distanciaBetweenFirstAndLast,// tamanho da trajetoria em linha reta
				subTrajOut.trajectoryDistance,// tamanho da trajetoria
				subTrajOut.GetInterval(),
				subTrajOut.GetSpeed(),
				String.format(EXECUTE_GET_DIRECTION_CHANGING, subTrajDirTar.firstGID, subTrajDirTar.lastGid, subTrajOut.firstGID, subTrajOut.lastGid),
				isUnusual,
				auxTid,
				auxGidObst,
				tidRecurrence);
		
		if(!isUnusual){
			sql = "update "+TRAJ_DATA_TABLE_NAME+ " set "
				+ "s_first_gid = %d, s_last_gid = %d, s_distance = %s, s_duration = %s, s_speed = %s "
				+ "where tid = %d and target_gid = %d and tid_recurrence = %d;";
			
			query = query + String.format(sql, 
					s.firstGID, // armazena Gid inicial da subtrajet�ria direcionada ao alvo e seu tamanho
					s.lastGid, // armazena Gid final da subtrajet�ria direcionada
					s.trajectoryDistance,// tamanho da trajetoria
					s.GetInterval(),
					s.GetSpeed(),
					auxTid,
					auxGidObst,
					tidRecurrence);
		}
		this.getUnusualBehaviorsInformation.execute(query);
	}
	
	public void ComputeUnusualBehavior(int auxTid, int auxGidObst, double localScore, String unusualBehavior, int tidRecurrence) throws SQLException
	{
		String sql = "insert into %s values (%d,%d,%d, '%s',%s);";
		String query = String.format(sql, UNUSUAL_BEHAVIORS_TABLE_NAME, auxTid, auxGidObst, tidRecurrence,unusualBehavior,localScore); 
		this.getTrajectoriesDataIntoROI.execute(query);
				
	}
	
	//this was made to set the is_unusual_behavior column to false
	//for those sub-trajectories that enter the ROI and do not present any unusual behavior
	//in order to be possible to count them on GLOBAL SCORE calculation
	public void UpdateToNoneBehavior(int auxGidObst, int tidRecurrence) throws SQLException
	{
		String sql = "update %s set is_unusual_behavior = false where tid_recurrence = %d and target_gid = %d;";
		String query = String.format(sql, TRAJ_DATA_TABLE_NAME, tidRecurrence, auxGidObst); 
		this.getTrajectoriesDataIntoROI.execute(query);		
	}
	
	public ResultSet GetUnusualBehavior() throws SQLException
	{
		String sql = "select tid, target_gid, tid_recurrence, sin_dist, sin_linedist, (sout_speed-sin_speed) as speed_difference, "
				+ " s_duration, angle, o.speed_threshold, o.duration_threshold from %s "
				+ " inner join %s o on o.gid = target_gid"
				+ " where is_unusual_behavior order by target_gid, tid";
		String query = String.format(sql, TRAJ_DATA_TABLE_NAME, parametros.tableObjectCoordinatesName);
		return this.getUnusualBehaviorsInformation.executeQuery(query);
	}
	
	public ResultSet GetTrajectoriesUnusualPerTargetObjects() throws SQLException{
		String sql = "select distinct tid, target_gid from %s";
		String query = String.format(sql, UNUSUAL_BEHAVIORS_TABLE_NAME);
		return this.getUnusualBehaviorsInformation.executeQuery(query);
	}
	
	public ArrayList<Integer> GetTargetObjects() throws SQLException{
		String sql = "select * from "+TARGET_OBJECT_TABLE_NAME;
		ResultSet objs = this.getResults.executeQuery(sql);
		ArrayList<Integer> targetsGid = new ArrayList<Integer>();
		while(objs.next()){
			targetsGid.add(objs.getInt("gid"));
		}
		return targetsGid;
	}
	
	public ResultSet GetRanking() throws SQLException{
		String sql = "select * from "+RANKING_TABLE_NAME+" union "+
				"select distinct t.tid, 0 from "+TRAJ_DATA_TABLE_NAME+" t where not t.tid in " +
				"(select tid from "+RANKING_TABLE_NAME+") order by global_score desc";
		return this.getResults.executeQuery(sql);
	}
		
	public void Close() throws SQLException{
		this.getUnusualBehaviorsInformation.close();
		this.getResults.close();
		this.getSubTrajectoryMeasures.close();
		this.getTrajectoriesIntoROI.close();
		this.getTrajectoriesDataIntoROI.close();
		this.banco.CloseConnection();
	}
	
	//////////////////////////////////////////////////////////////////////////////
	//avoidance create confidence incremental region
	//////////////////////////////////////////////////////////////////////////////
	public int GetPointBeforeGetIntoROI(int minGidMaxDist, int auxTid, int auxGidObst, int tidRecurrence) throws SQLException{
		String sql = "select max(t.gid) as gid from "+parametros.tableTrajectoriesName+" t " +
						"inner join "+TRAJ_DATA_TABLE_NAME+" a on a.tid = "+auxTid+ " and a.tid_recurrence = "+tidRecurrence+
						" where t.gid < a.thisgid"+
						" and t.tid= "+ auxTid+
						" and not st_intersects(t.the_geom, "+
						this.GetBufferForInterestRegion(""+auxGidObst)+")";
		ResultSet rs = this.getSubTrajectoryMeasures.executeQuery(sql);
        if(rs.next())
        	return rs.getInt("gid");
        return -1;
	}
}

package Persistencia;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Negocio.Parametros;

public class ImportTrajetories {

	private String line = "";
	private String cvsSplitBy = ";";

	private DBConnection banco;

	private static String TRAJETORY_COLUMNS = "gid serial, tid int8, time timestamp with time zone, velocity float, CONSTRAINT gid_key_%s PRIMARY KEY (gid)";
	private static String INSERT_TRAJECTORY_WITH_POINT = "INSERT INTO %s (tid, time, velocity, the_geom) VALUES (%d, '%s', %s, ST_GeomFromText('POINT(%s %s)'));";
	private static String INSERT_TRAJECTORY_WITH_STRING_GEOM = "INSERT INTO %s (tid, time, velocity, the_geom) VALUES (%d, '%s', %s, '%s');";
	private static String DELETE_TRAJECTORIES = "DELETE FROM %s WHERE tid = %d;";
	
	private static String DEVICE_TRAJETORY_FILE_COLUMNS = "device_id int8, trajectory_id int8, file_name text, CONSTRAINT constraine_%s unique (device_id, trajectory_id, file_name)";
	private static String INSERT_ASSOCIATOR_COLUMNS = "INSERT INTO %s (device_id, trajectory_id, file_name) VALUES (%d, %d, '%s');";
	private static String DELETE_ASSOCIATION = "DELETE FROM %s WHERE file_name = '%s';";
	
	
	private String associatorTableName; 
	private String trajectoriesTableName;
	
	private ArrayList<String> errorMessages;
	
	private Parametros param;

	public ImportTrajetories(Parametros p, String fileName) throws ClassNotFoundException, SQLException, IOException {
		this.param = p;
		this.errorMessages = new ArrayList<String>();
		this.SetTrajectoriesTableName(p.tableTrajectoriesName);
		this.SetAssociatorTableName(p.tableTrajectoriesName);
		this.banco = DBConnection.GetInstance(p.databaseName, p.username, p.password);
		this.CreateTableIfNotExists(p.tableTrajectoriesName, String.format(TRAJETORY_COLUMNS,p.tableTrajectoriesName), true);
		this.CreateTableIfNotExists(associatorTableName, String.format(DEVICE_TRAJETORY_FILE_COLUMNS, p.tableTrajectoriesName), false);
		this.InsertAllTrajectories(p.trackPath+fileName);
	}
	private void SetAssociatorTableName(String tableName){
		this.associatorTableName = String.format("device_%s_file", tableName);
	}
	
	private void SetTrajectoriesTableName(String tableName){
		this.trajectoriesTableName = tableName;
	}
	
	public ArrayList<String> GetErrorMessages(){
		return this.errorMessages;
	}

	private void CreateTableIfNotExists(String tableName, String columns, boolean addTheGeom) throws SQLException {
		if (!TableExists(tableName)) {
			String sql = "Create table " + tableName + "(" + columns + ")";
			Statement checkExistence = this.banco.CreateStatement();
			checkExistence.execute(sql);
			if(addTheGeom)
				this.banco.AddGeometryColumnToTable(tableName, "the_geom", "POINT", "2");
		}
	}

	private boolean TableExists(String tableName) throws SQLException {
		Statement checkExistence = this.banco.CreateStatement();
		String sql = "select count(1) from (select * from pg_tables where schemaname='public') as tables where tablename = '"+ tableName + "'";
		ResultSet queryResult = checkExistence.executeQuery(sql);
		queryResult.next();
		return queryResult.getInt("count") > 0;
	}

	private void InsertAllTrajectories(String trajectoriesFileNames) throws IOException, SQLException {

		BufferedReader br = new BufferedReader(new FileReader(trajectoriesFileNames));
		String[] row;
		int deviceId, trajectoryId;
		String fileName;
		boolean isGeomPassedAsPoint = Boolean.parseBoolean(br.readLine());
		while ((line = br.readLine()) != null) {
			if (line.charAt(0) != '-') {
				row = line.split(";");
				deviceId = Integer.parseInt(row[0]);
				trajectoryId = Integer.parseInt(row[1]);
				fileName = row[2];
				if(InsertDeviceTrajectoryFile(deviceId, trajectoryId, fileName)){
					if(!InsertSingleTrajectory(param.trackPath+row[2], trajectoryId, fileName, isGeomPassedAsPoint)){
						DeleteTrajectoryBadData(fileName, trajectoryId);
					}
					else
					{
						this.errorMessages.add(String.format("%s file was imported sucessfully.", fileName));
					}
				}
				else
				{
					this.errorMessages.add(String.format("%s file was already uploaded.", fileName));
				}
			}
		}
	}

	private String GetFileNameFromPath(String path) {
		String[] pathSplit = path.split("/");
		return pathSplit[pathSplit.length - 1];
	}

	private boolean InsertDeviceTrajectoryFile(int deviceId, int trajectoyId, String fileName) {
		String sql = String.format(INSERT_ASSOCIATOR_COLUMNS, associatorTableName, deviceId, trajectoyId, fileName);
		Statement insert;
		try {
			insert = this.banco.CreateStatement();
			insert.execute(sql);
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	private boolean InsertSingleTrajectory(String path, int trajectoryId, String fileName, boolean isGeomPassedAsPoint) {
		int count = 0;

		String query = "";
		int point_id;
		String time, velocity, latitude, longitude, theGeom;
		Statement insert;
		try {
			insert = this.banco.CreateStatement();
			BufferedReader br = new BufferedReader(new FileReader(path));
			String[] row;
			while ((line = br.readLine()) != null) {
				// use comma as separator
				row = line.split(cvsSplitBy);
				if(isGeomPassedAsPoint){
					time = row[8];
					velocity = row[7].equals("") ? "0" : row[7];
					latitude = row[2];
					longitude = row[3];
					query = query.concat(String
							.format(INSERT_TRAJECTORY_WITH_POINT, this.trajectoriesTableName, trajectoryId, time, velocity, latitude, longitude));
				}else{
					time = row[3];
					velocity = "0";
					theGeom = row[4];
					query = query.concat(String
							.format(INSERT_TRAJECTORY_WITH_STRING_GEOM, this.trajectoriesTableName, trajectoryId, time, velocity, theGeom));
				}

				count++;
				if (count == 100) {
					insert.execute(query);
					count = 0;
					query = "";
				}
			}
			if (!query.equals(""))
				insert.execute(query);
		} catch (SQLException e) {
			this.errorMessages.add(String.format("\n%s has error: Data is not formated properly!\n", fileName));
			return false;
		} catch (FileNotFoundException e) {
			this.errorMessages.add(String.format("%s has error: File not found!\n", fileName));
			return false;
		} catch (NumberFormatException e) {
			this.errorMessages.add(String.format("%s has error: First two columns of a trejctory must  integer!\n", fileName));
			return false;
		} catch (IOException e) {
			this.errorMessages.add(String.format("%s has error: Error when read a line of the file!\n", fileName));
			return false;
		}
		return true;
	}
	
	public void DeleteTrajectoryBadData(String fileName, int trajectoryId) throws SQLException{
		String sql = String.format(DELETE_ASSOCIATION, associatorTableName, fileName);
		sql = sql.concat(String.format(DELETE_TRAJECTORIES, this.trajectoriesTableName, trajectoryId));
		Statement delete = this.banco.CreateStatement();
		delete.execute(sql);
	}

}
